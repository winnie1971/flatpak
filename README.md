Fix Flatpak Theming in KDE Plasma

To use the above script, do the following:

    Save the above file to your home folder or a folder of your choice.
    Open a terminal window and navigate to where you saved the above script to.
    Make your new script executable by typing: chmod a+x flatpakthemefix.sh
    Run the script by typing ./flatpakthemefix.sh

You will only have to do one the above solutions right after installing a Flatpak. Updating the Flatpak will not undo the changes done.
